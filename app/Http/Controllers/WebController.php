<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class WebController extends Controller
{
    public function home (){
        return view('home',[
            "title"=> "home",
            "nama" => "I NYOMAN ADITYA TRIGUNA",
            "no" => "+62 81953715520",
            "email" => "inyomanaditya1774@gmail.com",
            "alamat" => "JLN. NUSA INDAH RAYA NO.81"]);
    }
    public function contact (){
        return view('contact',[
            "title"=> "contact",]);
    }
    public function about (){
        return view('about',[
            "title"=> "about",]);
    }
    public function services (){
        return view('services',[
            "title"=> "services",]);
    }
    public function achiev (){
        return view('achiev',[
            "title"=> "achiev",]);    
    }
    public function blog (){
        return view('blog',["title"=> "blog",]);    
    }
      public function fallback (){
        return 'Halaman tidak ditemukan';    
    }
}
