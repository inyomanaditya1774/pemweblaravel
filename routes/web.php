<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controller\WebController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [WebController::class, 'home']);

Route::get('/contact',[WebController::class, 'contact']);

Route::get('/about', [WebController::class, 'about']);

Route::get('/services', [WebController::class, 'services']);

Route::get('/achiev', [WebController::class, 'achiev']);

Route::get('/blog',[WebController::class, 'blog']);

Route::fallback([WebController::class, 'fallback']);
