<!-- portfolio -->
<div class="portfolio" id="port">
				<div class="service-head text-center">
						<h4>Prestasi</h4>
						<h3><span>PORTFOLIO</span></h3>
						<span class="border"></span>
					</div>
			<div class="portfolio-grids">
				<script src="js/easyResponsiveTabs.js" type="text/javascript"></script>
				<script type="text/javascript">
									$(document).ready(function () {
										$('#horizontalTab').easyResponsiveTabs({
											type: 'default', //Types: default, vertical, accordion
											width: 'auto', //auto or any width like 600px
											fit: true   // 100% fit in a container
										});
									});

				</script>
				<div class="sap_tabs">
					<div id="horizontalTab" style="display: block; width: 100%; margin: 0px;">
						<ul class="resp-tabs-list">
							<li class="resp-tab-item" aria-controls="tab_item-0" role="tab"><span>Dokumentasi</span></li>

						</ul>
						<div class="resp-tabs-container">
							<div class="tab-1 resp-tab-content" aria-labelledby="tab_item-0">
								<div class="col-md-3 team-gd ">

										<a href="#portfolioModal1" class="portfolio-link b-link-diagonal b-animate-go" data-toggle="modal"><img src="images/P1.jpg" alt="">

										</a>

								</div>
								<div class="col-md-3 team-gd ">

										<a href="#portfolioModal3" class="portfolio-link b-link-diagonal b-animate-go" data-toggle="modal"><img src="images/P2.jpg" alt="">

										</a>

								</div>
								<div class="col-md-3 team-gd ">

										<a href="#portfolioModal2" class="portfolio-link b-link-diagonal b-animate-go" data-toggle="modal"><img src="images/P3.jpg" alt="">

										</a>

								</div>
								<div class="col-md-3 team-gd ">

										<a href="#portfolioModal4" class="portfolio-link b-link-diagonal b-animate-go" data-toggle="modal"><img src="images/P4.jpg" alt="">

										</a>

								</div>
								<div class="col-md-3 team-gd yes_marg ">

										<a href="#portfolioModal5" class="portfolio-link b-link-diagonal b-animate-go" data-toggle="modal"><img src="images/P5.jpg" alt="">

										</a>

								</div>
								<div class="col-md-3 team-gd yes_marg ">

										<a href="#portfolioModal6" class="portfolio-link b-link-diagonal b-animate-go" data-toggle="modal"><img src="images/P6.jpg" alt="">

										</a>

								</div>
								<div class="col-md-3 team-gd yes_marg ">

										<a href="#portfolioModal7" class="portfolio-link b-link-diagonal b-animate-go" data-toggle="modal"><img src="images/P7.jpg" alt="">

										</a>

								</div>
								<div class="col-md-3 team-gd yes_marg ">

										<a href="#portfolioModal8" class="portfolio-link b-link-diagonal b-animate-go" data-toggle="modal"><img src="images/P8.jpg" alt="">

										</a>

								</div>
								<div class="clearfix"></div>
							</div>

								<div class="clearfix"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
	</div>
